<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container knowledge-king" role="main">
		<div class="header-page-banner mb-0">
			<div class="inner">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-xl-10">
							<h1 class="head-page txtC">คลังความรู้</h1>
							<ul class="page-tab-selection">
								<li class="active"><a href="knowledge-king.php">ศาสตร์พระราชา</a></li>
								<li><a href="knowledge-study.php">ถอดบทเรียนจากโครงการ</a></li>
								<li><a href="knowledge-local.php">ภูมิปัญญาท้องถิ่น</a></li>
							</ul>
							<div class="page-intro">
								<div class="box-img">
									<div class="img-item">
										<img class="img" src="assets/img/knowledge-king-thumb.jpg" alt="">
										<img class="txt" src="assets/img/txt-arrow_knowledge-king.png" alt="ศาสตร์พระราชา">
									</div>
									<img class="aw" src="assets/img/aw_intro-knowledge-king.png"/>
								</div>
								<div class="box-txt">
									<h2 class="head">ศาสตร์พระราชา</h2>
									<p class="summary">
										<strong>โครงการ “พลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของ
										แผ่นดิน”</strong> ก่อตั้งขึ้นในปี พ.ศ. 2556  โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน พระบาทสมเด็จพระปรมินทรมหาภูมิพล
										อดุลยเดช ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ
										ป่าสัก ก่อตั้งขึ้นในปี พ.ศ. 2556 พระบาทสมเด็จพระปรมินทรมหาภูมิ
										พลอดุลยเดช ที่ทรงห่วงใยต่อปัญหา โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="line-divider d-block d-lg-none"></div>
			<br class="d-none d-lg-block">
		</div>
		<div class="part part-1">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-7 col-xl-6">
						<p>
							ก่อตั้งขึ้นในปี พ.ศ. 2556  โดยได้รับแรงบันดาลใจจากกระแสพระราช
							ดำรัสในพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช ที่ทรงห่วงใย
							ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำป่าสัก อดุลยเดช ที่ทรงห่วงใย
							ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหา
							น้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณ
							ลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ
						</p>
						<p>
							ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหา
							น้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณ
							ลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ โดยได้รับแรงบันดาลใจจากกระแสพระราชดำรัสในพระบาทสมเด็จ
						</p>
					</div>
					<div class="box-img col-12 col-lg-5 col-xl-6">
						<img src="assets/img/knowledge-king-img.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="part part-2">
			<div class="container">
				<div class="row">
					<div class="box-img col-12 col-lg-5 col-xl-6">
						<img src="assets/img/knowledge-king-img-02.png" alt="">
					</div>
					<div class="col-12 col-lg-7 col-xl-6">
						<p>
							เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="part part-3">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-7 col-xl-6">
						<br class="d-none d-xl-block">
						<br class="d-none d-xl-block">
						<br class="d-none d-xl-block">
						<br class="d-none d-xl-block">
						<br class="d-none d-xl-block">
						<p>
							ก่อตั้งขึ้นในปี พ.ศ. 2556  โดยได้รับแรงบันดาลใจจากกระแสพระราช
							ดำรัสในพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช ที่ทรงห่วงใย
							ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำป่าสัก อดุลยเดช ที่ทรงห่วงใย
							ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหา
							น้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณ
							ลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ
						</p>
						<p>
							ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหา
							น้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ ต่อปัญหาน้ำท่วมและภัยแล้งบริเวณ
							ลุ่มน้ำป่าสัก ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ โดยได้รับแรงบันดาลใจจากกระแสพระราชดำรัสในพระบาทสมเด็จ
						</p>
					</div>
					<div class="box-img col-12 col-lg-5 col-xl-6">
						<img src="assets/img/knowledge-king-img-03.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>