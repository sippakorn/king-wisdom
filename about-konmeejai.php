<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<div class="header-page-banner">
			<div class="inner">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-xl-10">
							<h1 class="head-page txtC">รู้จักโครงการ</h1>
							<ul class="page-tab-selection">
								<li><a href="about-pakee.php">ภาคีเครือข่าย</a></li>
								<li class="active"><a href="about-konmeejai.php">คนมีใจ</a></li>
							</ul>
							<div class="page-intro">
								<div class="box-img">
									<div class="img-item">
											<img class="img" src="assets/img/about-konmeejai-thumb.jpg" alt="">
											<img class="txt" src="assets/img/txt-arrow_konmeejai.png" alt="คนมีใจ">
									</div>
									<img class="aw" src="assets/img/aw_intro-about-konmeejai.png"/>
								</div>
								<div class="box-txt">
									<h2 class="head">คนมีใจ</h2>
									<p class="summary">
										<strong>โครงการ “พลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของ
										แผ่นดิน”</strong> ก่อตั้งขึ้นในปี พ.ศ. 2556  โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน พระบาทสมเด็จพระปรมินทรมหาภูมิพล
										อดุลยเดช ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ
										ป่าสัก ก่อตั้งขึ้นในปี พ.ศ. 2556 พระบาทสมเด็จพระปรมินทรมหาภูมิ
										พลอดุลยเดช ที่ทรงห่วงใยต่อปัญหา โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="article-container">
						<div class="row">
							<?php $data = array('อาจารย์ สุขะชัย ศุภศิร','แพทย์หญิง ราตรี เอื้อศิริ','ดร. เกริก มีมุ่งกิจ','พระวีรยุทธ อภิวีโร','นายสุ่น วงศ์ซิ้ม','อาจารย์สาคร โรจน์คำลือ','พระวีรยุทธ อภิวีโร','นายสุ่น วงศ์ซิ้ม','อาจารย์สาคร โรจน์คำลือ');
								foreach ($data as $k => $v) {
							?>
							<div class="col-12 col-lg-4">
								<article class="article-item">
									<a href="about-detail.php">
										<span class="box-img">
											<img src="assets/img/_demo/about-konmeejai-thumb-0<?php echo $k+1; ?>.jpg" alt="<?php echo $v; ?>">
										</span>
										<span class="box-txt border">
											<h2 class="headline"><?php echo $v; ?></h2>
											<p class="summary">
												จิตพิสัยคันยิกรรมาชนวาไรตี้อพาร์ทเมนท์บลูเบอร์รีโครนาเซลส์อยุติธรรมฮิต ซิ่งซีดานซาร์ปอดแหก
												โปรเจกต์
											</p>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</a>
								</article>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<nav class="pagination-nav">
				<div class="row">
					<div class="col-auto"><a href="#" class="btn-arrow-prev"></a></div>
					<div class="col">
						<ul class="page">
							<li class="aciive"><span>1</span></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
						</ul>
					</div>
					<div class="col-auto"><a href="#" class="btn-arrow-next"></a></div>
				</div>
			</nav>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>