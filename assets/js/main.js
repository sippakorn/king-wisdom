$(document).ready(function(){
	$app.check.init();
	$app.global.init();
});

/* =================================
	variable x function
================================= */
var $app = {};
$app.check = {
	init : function(){
		//this.os();
		//this.ie();
	},
	/* check device */
	device : function(){
		if(!$.browser.mobile && navigator.userAgent.match(/iPad/i) == null){
			return 'desktop';
		} else if(navigator.userAgent.match(/iPad/i) != null){
			return 'tablet';
		} else if($.browser.mobile){
			return 'mobile';
		}
	},
	/* check os */
	os : function() {
		var userAgent = window.navigator.userAgent,
			platform = window.navigator.platform,
			macOS = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
			windows = ['Win32', 'Win64', 'Windows', 'WinCE'],
			iOS = ['iPhone', 'iPad', 'iPod'],
			OS;

			if (macOS.indexOf(platform) !== -1) {
				OS = 'isMac';
			} else if (iOS.indexOf(platform) !== -1) {
				OS = 'isiOS';
			} else if (windows.indexOf(platform) !== -1) {
				OS = 'isWindows';
			} else if (/Android/.test(userAgent)) {
				OS = 'isAndroid';
			} else if (!$check.os && /Linux/.test(platform)) {
				OS = 'isLinux';
			}
			$('html').addClass(OS);
			return OS;
	},
	/* check ie */
	ie : function(){
		if($.browser.msie && $.browser.version<=10){
			return true;
			$("html").addClass("isIE");
		} else {
			return false;
		}
	},
}
$app.global = {
	init : function(){
		this.menu();
		this.tab();
		this.selectbox();
		$('.fancybox-inline').click(function(e){
			$.fancybox({
				href:$(this).attr('href'),
				margin:[50,20,20,20]
			});
			e.preventDefault();
		});
		$('.popup-vdo').click(function(e){
			$.fancybox({
				type:'iframe',
				href:$(this).attr('href'),
				margin:[50,20,20,20],
				padding:0
			});
			e.preventDefault();
		});
		if($app.check.device() !== 'mobile'){
			$('.popup-img').fancybox({
				padding:0,
				margin:[50,20,20,20]
			});
		}
	},
	menu : function(){
		$(".btn-menu").click(function() {
			$('html').addClass('show-menu');
		});
		$(".main-nav,.btn-menu-close").click(function() {
			$('html').removeClass('show-menu');
		});
		$('.main-header .btn-menu,.main-nav .inner').click(function(e){
			e.stopPropagation();
		});
	},
	tab : function(){
		var now_tab = 0;
		if($('.tabbox').length>0){
			$('.tabbox').each(function(){
				$(this).find('.tb-select li').removeClass('active');
				$(this).find('.tb-item').removeClass('active').hide();
				$(this).find('.tb-select li').eq(now_tab).addClass('active');
				$(this).find('.tb-item').eq(now_tab).addClass('active').show();
			});
			$('.tb-select li a').click(function(e) {
				now_tab = $(this).parent().index();
				$(this).parents('.tabbox').find('.tb-select li').removeClass('active');
				$(this).parents('.tabbox').find('.tb-select li').eq(now_tab).addClass('active');
				$(this).parents('.tabbox').find('.tb-item').removeClass('active').hide();
				$(this).parents('.tabbox').find('.tb-item').eq(now_tab).addClass('active').show();
				e.preventDefault();
			});
		}
	},
	selectbox : function(){
		if($('.selectbox').length>0){
			$('html').click(function(){
				$('.selectbox').removeClass('show-options');
			});
			$('.selectbox .sb-selector').click(function(e){
				if($(this).parent('.selectbox').hasClass('show-options')){
					$(this).parent('.selectbox').removeClass('show-options');
				} else {
					$(this).parent('.selectbox').addClass('show-options');
				}
				e.stopPropagation();
			});
			$('.selectbox .sb-options a').click(function(e){
				var val = $(this).attr('data-value');
				var txt = $(this).text();
				$(this).parents('.selectbox').find('.sb-selector').text(txt).addClass('selected');
				$(this).parents('.selectbox').removeClass('show-options');
				$(this).parents('.selectbox').find('[name="sb-value"]').val(val);
				e.preventDefault();
			});
		}
	},
}