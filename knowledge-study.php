<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<div class="header-page-banner">
			<div class="inner">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-xl-10">
							<h1 class="head-page txtC">คลังความรู้</h1>
							<ul class="page-tab-selection">
								<li><a href="knowledge-king.php">ศาสตร์พระราชา</a></li>
								<li class="active"><a href="knowledge-study.php">ถอดบทเรียนจากโครงการ</a></li>
								<li><a href="knowledge-local.php">ภูมิปัญญาท้องถิ่น</a></li>
							</ul>
							<div class="page-intro">
								<div class="box-img">
									<div class="img-item">
										<img class="img" src="assets/img/knowledge-study-thumb.jpg" alt="">
										<img class="txt" src="assets/img/txt-arrow_knowledge-study.png" alt="ถอดบทเรียนจากโครงการ">
									</div>
									<img class="aw" src="assets/img/aw_intro-knowledge-study.png"/>
								</div>
								<div class="box-txt">
									<h2 class="head">ถอดบทเรียนจากโครงการ</h2>
									<p class="summary">
										<strong>โครงการ “พลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของ
										แผ่นดิน”</strong> ก่อตั้งขึ้นในปี พ.ศ. 2556  โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน พระบาทสมเด็จพระปรมินทรมหาภูมิพล
										อดุลยเดช ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ
										ป่าสัก ก่อตั้งขึ้นในปี พ.ศ. 2556 พระบาทสมเด็จพระปรมินทรมหาภูมิ
										พลอดุลยเดช ที่ทรงห่วงใยต่อปัญหา โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="article-container">
						<div class="row">
							<?php $data = array('หลุมขนมครก','โคกหนองนา','ภูมิสังคม','บันได 9 ชั้น','เกษตรอินทรีย์','ทำนบกั้นน้ำ','จักรสาน','ปลูกข้าวอินทรีย์','เกษตรหมุนเวียน');
								foreach ($data as $k => $v) {
							?>
							<div class="col-12 col-lg-4">
								<article class="article-item">
									<a href="knowledge-study-detail.php">
										<span class="box-img">
											<img src="assets/img/_demo/knowledge-study-thumb-0<?php echo $k+1; ?>.jpg" alt="<?php echo $v; ?>">
										</span>
										<span class="box-txt border">
											<h2 class="headline"><?php echo $v; ?></h2>
											<p class="summary">
												จิตพิสัยคันยิกรรมาชนวาไรตี้อพาร์ทเมนท์บลูเบอร์รีโครนาเซลส์อยุติธรรมฮิต ซิ่งซีดานซาร์ปอดแหก
												โปรเจกต์
											</p>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</a>
								</article>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<nav class="pagination-nav">
				<div class="row">
					<div class="col-auto"><a href="#" class="btn-arrow-prev"></a></div>
					<div class="col">
						<ul class="page">
							<li class="aciive"><span>1</span></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
						</ul>
					</div>
					<div class="col-auto"><a href="#" class="btn-arrow-next"></a></div>
				</div>
			</nav>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>