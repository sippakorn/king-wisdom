<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<nav class="breadcrumb">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-lg-8">
						<a class="item" href="index.php">หน้าหลัก</a>
						<a class="item" href="event.php">ข่าวสารและกิจกรรม</a>
						<span class="item">ปลูกป่าทดแทนจังหวัดน่าน ครั้งที่ 3</span>
					</div>
				</div>
			</div>
		</nav>
		<article class="main-article">
			<section class="section">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-8">
							<figure class="main-img">
								<img src="assets/img/_demo/event-detail.jpg" alt="">
							</figure>
							<header class="header">
								<div class="row">
									<div class="col-12 col-md-auto">
										<div class="box-date-blue">30 เม.ย. 2561</div>
									</div>
									<div class="col-12 col-md-auto">
										<div class="hgroup">
											<h1 class="headline">ปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3</h1>
											<p class="txt">Today 6 PM · Yim Yam Hostel & Garden · Bangkok, Thailand 10220</p>
										</div>
									</div>
								</div>
							</header>
							<div class="body">
								<!-- text editor here -->
								<p>
									เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน
								</p>
								<!-- / text editor here -->
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="section section-map">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-8">
							<h2 class="head txtC">แผนที่</h2>
							<div class="box-map">
								<img class="w-100" src="assets/img/_demo/map-02.jpg" alt="">
							</div>
							<p class="txtC">
								<a href="#" class="btn-red"><strong class="txt">นำทางใน Google</strong> <i class="fa fa-angle-double-right right" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</section>
			<section class="section">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-8">
							<h2 class="head txtC">เรื่องเล่าจากโครงการ</h2>
							<div class="body">
								<!-- text editor here -->
								<p>
									เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน
								</p>
								<!-- / text editor here -->
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="section section-event-gallery">
				<div class="container">
					<div class="row row-slider">
						<?php for ($i=0; $i<12 ; $i++) { ?>
						<div class="col-10 col-md-4 col-lg-3">
							<a href="media-detail.php" class="item"><img src="assets/img/_demo/event-gallery-thumb-0<?php echo $i+1; ?>.jpg" alt=""></a>
						</div>
						<?php } ?>
					</div>
				</div>
			</section>
		</article>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>