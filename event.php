<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container event" role="main">
		<section class="herobanner">
			<div class="event-slider-container">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-11">
							<div class="box-txt-overlay">
								<h2 class="head-section txt-head">กิจกรรมตามรอยพ่อ</h2>
								<p class="txt-white pl-lg-5 pr-lg-5">
									ในปีที่ 6 (พ.ศ. 2561) นับเป็นปีสุดท้ายของระยะที่ 2 ในแผนหลัก 9 ปี คือ ระยะแตกตัว โครงการฯ เน้นการขยายผลในรูปแบบของการ "แตกตัวทั่วไทย เอามื้อสามัคคี" โดยนำภารกิจการเอามื้อสามัคคี หรือการลงแขกช่วยเหลือกันในพื้นที่ต่างๆ ซึ่งเป็นประเพณีดั้งเดิมของคนไทย มาเป็นกลวิธีในการขับเคลื่อน โดยได้รับแรงบันดาลใจจากโครงการในพระราชดำริฯ มากำหนดพื้นที่เป้าหมาย "เอามื้อสามัคคี" เพื่อถ่ายทอดเรื่องราวตัวอย่างความสำเร็จของเครือข่าย หรือ "คนมีใจ" ที่เป็นต้นแบบในลุ่มน้ำต่างๆ ที่มีสภาพภูมิสังคมที่แตกต่างกัน เพื่อสร้างแรงบันดาลใจสู่สาธารณชนทั่วประเทศในการเดินตามรอยพ่อ สานต่อศาสตร์พระราชา และภูมิปัญญาท้องถิ่นต่อไปไม่สิ้นสุด
								</p>
							</div>
							<div class="event-slider">
								<div class="item">
									<article class="article-item pin-item nowrap">
										<a href="event-detail.php">
											<span class="box-img">
												<img src="assets/img/_demo/herobanner-article-0<?php echo $i+1; ?>.jpg" alt="" />
												<span class="date"><strong>30</strong> เม.ย. 2561</span>
											</span>
											<span class="box-txt">
												<h3 class="headline">ปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3</h3>
												<p class="summary">
													ติ่มซำคอลัมนิสต์บอดี้สึนามิเมจิกมวลชน เฟิร์มแมชชีนดาวน์ศิรินทร์ออร์แก
													นิค ปาสกาล ฮัลโลวีนสแตนดาร์ด
												</p>
												<p class="txt-readmore">
													<strong class="txt">ดูรายละเอียด</strong>
													<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
												</p>
											</span>
											<span class="badge now"><span class="txt">NEW</span></span>
										</a>
									</article>
								</div>
								<?php for ($i=0; $i < 4 ; $i++) { ?>
								<div class="item">
									<article class="article-item pin-item nowrap">
										<a href="event-detail.php">
											<span class="box-img">
												<img src="assets/img/_demo/herobanner-article-0<?php echo $i+1; ?>.jpg" alt="" />
												<span class="date"><strong>30</strong> เม.ย. 2561</span>
											</span>
											<span class="box-txt">
												<h3 class="headline">ปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3</h3>
												<p class="summary">
													ติ่มซำคอลัมนิสต์บอดี้สึนามิเมจิกมวลชน เฟิร์มแมชชีนดาวน์ศิรินทร์ออร์แก
													นิค ปาสกาล ฮัลโลวีนสแตนดาร์ด
												</p>
												<p class="txt-readmore">
													<strong class="txt">ดูรายละเอียด</strong>
													<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
												</p>
											</span>
											<span class="badge coming-soon"><span class="txt">COMING<br>SOON</span></span>
										</a>
									</article>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="event-timeline">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-10 offset-lg-1">
						<h2 class="head-section txt-head">ตัวอย่างความสำเร็จ</h2>
						<div class="timeline-container">
							<?php for ($i=0; $i < 4 ; $i++) { ?>
							<div class="timeline-item">
								<article class="article-item pin-item nowrap">
									<a href="event-done-detail.php">
										<span class="box-img">
											<img src="assets/img/_demo/herobanner-article-0<?php echo $i+1; ?>.jpg" alt="" />
											<span class="date"><strong>30</strong> เม.ย. 2561</span>
										</span>
										<span class="box-txt">
											<h3 class="headline">ปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3</h3>
											<p class="summary">
												ติ่มซำคอลัมนิสต์บอดี้สึนามิเมจิกมวลชน เฟิร์มแมชชีนดาวน์ศิรินทร์ออร์แก
												นิค ปาสกาล ฮัลโลวีนสแตนดาร์ด
											</p>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</a>
								</article>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php include('inc/javascript.php'); ?>
<script>
	$(document).ready(function(){
		$('.herobanner .event-slider').slick({
		  infinite:true,
		  arrows:true,
		  dots: true,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 576,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		  ],
		});
	});
</script>
<?php include('inc/footer.php'); ?>