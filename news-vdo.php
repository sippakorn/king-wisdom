<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container news-list" role="main">
		<div class="header-page-banner style2">
			<div class="inner">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-xl-10">
							<h1 class="head-page txtC">สื่อประชาสัมพันธ์</h1>
							<ul class="page-tab-selection">
								<li><a href="news-pr.php">PRESS RELEASE</a></li>
								<li class="active"><a href="news-vdo.php">VDO CONTENT</a></li>
							</ul>
							<ul class="tag-list">
								<li><a href="news-vdo-data.php">ศาสตร์พระราชา</a></li>
								<li><a href="news-vdo-data.php">ดร.เกริก</a></li>
								<li><a href="news-vdo-data.php">คำสอน</a></li>
								<li><a href="news-vdo-data.php">พอเพียง</a></li>
								<li><a href="news-vdo-data.php">ศาสตร์พระราชา</a></li>
								<li><a href="news-vdo-data.php">คำสอน</a></li>
								<li><a href="news-vdo-data.php">พอเพียง</a></li>
								<li><a href="news-vdo-data.php">ศาสตร์พระราชา</a></li>
								<li><a href="news-vdo-data.php">พอเพียง</a></li>
								<li><a href="news-vdo-data.php">คำสอน</a></li>
								<li><a href="news-vdo-data.php">ศาสตร์พระราชา</a></li>
								<li><a href="news-vdo-data.php">พอเพียง</a></li>
								<li><a href="news-vdo-data.php">คำสอน</a></li>
								<li><a href="news-vdo-data.php">คำสอน</a></li>
								<li><a href="news-vdo-data.php">ศาสตร์พระราชา</a></li>
								<li><a href="news-vdo-data.php">คำสอน</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<br>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="article-container row" id="article-container">
						<!-- load data via ajax -->
					</div>
				</div>
			</div>
			<nav class="pagination-nav">
				<div class="row">
					<div class="col-auto"><a href="#" class="btn-arrow-prev"></a></div>
					<div class="col">
						<ul class="page">
							<li class="aciive"><span>1</span></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
						</ul>
					</div>
					<div class="col-auto"><a href="#" class="btn-arrow-next"></a></div>
				</div>
			</nav>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<script>
	var dataContainer = $('#article-container');
	var isLoading = false;
	var timeout;
	$(document).ready(function(){
		// default load data
		fn_loadData('news-vdo-data.php');
		// click to load more data
		$('.tag-list a').click(function(e){
			var data_url = $(this).attr('href');
			if(!$(this).parent('li').hasClass('active')){
				if(!isLoading){
					isLoading = true;
					clearTimeout(timeout);
					$('.tag-list li.active').removeClass('active');
					$(this).parent('li').addClass('active');
					fn_loadData(data_url);
				}
			}
			e.preventDefault();
		});
	});
	// function load data
	function fn_loadData(data_url){
		dataContainer.addClass('loading');
		$.post(data_url,function(data){
    	setTimeout(function(){
				dataContainer.removeClass('loading');
	    	dataContainer.html(data);
    	},300);
    	timeout = setTimeout(function(){
    		var total = dataContainer.find('.article-item').length;
				for (var i = 0; i < total; i++) {
					dataContainer.find('.article-item').eq(i).delay(i*80).queue(function(){
						$(this).addClass('show');
						$(this).dequeue();
					});
				}
	    	isLoading = false;
	    	clearTimeout(timeout);
    	},400);
		});
	}
</script>
<?php include('inc/footer.php'); ?>