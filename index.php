<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container home" role="main">
		<section class="herobanner">
			<div class="img-slider">
				<div class="item">
					<img src="assets/img/herobanner.jpg" alt=""/>
				</div>
				<div class="item">
					<img src="assets/img/herobanner.jpg" alt=""/>
				</div>
			</div>
			<div class="event-slider-container">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-11">
							<h2 class="head-section txt-head">กิจกรรมตามรอยพ่อ</h2>
							<div class="event-slider">
								<?php for ($i=0; $i < 4 ; $i++) { ?>
								<div class="item">
									<article class="article-item pin-item nowrap">
										<a href="event-detail.php">
											<span class="box-img">
												<img src="assets/img/_demo/herobanner-article-0<?php echo $i+1; ?>.jpg" alt="" />
												<span class="date"><strong>30</strong> เม.ย. 2561</span>
											</span>
											<span class="box-txt">
												<h3 class="headline">ปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3</h3>
												<p class="summary">
													ติ่มซำคอลัมนิสต์บอดี้สึนามิเมจิกมวลชน เฟิร์มแมชชีนดาวน์ศิรินทร์ออร์แก
													นิค ปาสกาล ฮัลโลวีนสแตนดาร์ด
												</p>
											</span>
										</a>
									</article>
								</div>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ABOUT US -->
		<section class="section">
			<div class="row no-gutters">
				<div class="col-12">
					<h2 class="head-section">รู้จักโครงการ</h2>
					<div class="content">
						<div class="row no-gutters">
							<div class="col-12 col-md-6">
								<a href="about-pakee.php" class="article-banner">
									<span class="box-img" style="background-image:url('assets/img/home-about-thumb-01.jpg');"></span>
									<span class="box-txt">
										<span class="inner">
											<h3 class="headline txt-head">ภาคีเครือข่าย</h3>
											<span class="divider-h"></span>
											<p class="summary">
												จิตพิสัยคันยิกรรมาชนวาไรตี้อพาร์ทเมนท์<br>
												บลูเบอร์รีโครนาเซลส์อยุติธรรมฮิต ซิ่งซี<br>
												ดานซาร์ปอดแหกโปรเจกต์
											</p>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</span>
								</a>
							</div>
							<div class="col-12 col-md-6">
								<a href="about-konmeejai.php" class="article-banner">
									<span class="box-img" style="background-image:url('assets/img/home-about-thumb-02.jpg');"></span>
									<span class="box-txt">
										<span class="inner">
											<h3 class="headline txt-head">คนมีใจ</h3>
											<span class="divider-h"></span>
											<p class="summary">
												จิตพิสัยคันยิกรรมาชนวาไรตี้อพาร์ทเมนท์<br>
												บลูเบอร์รีโครนาเซลส์อยุติธรรมฮิต ซิ่งซี<br>
												ดานซาร์ปอดแหกโปรเจกต์
											</p>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- / ABOUT US -->
		<!-- KNOWLEDGE -->
		<section class="section">
			<div class="row no-gutters">
				<div class="col-12">
					<h2 class="head-section">คลังความรู้</h2>
					<div class="content">
						<div class="row no-gutters">
							<div class="col-12 col-md-6">
								<a href="knowledge-king.php" class="article-banner x-sm-half">
									<span class="box-img" style="background-image:url('assets/img/home-knowledge-thumb-03.jpg');"></span>
									<span class="box-txt">
										<span class="inner">
											<h3 class="headline txt-head">ศาสตร์พระราชา</h3>
											<span class="divider-h"></span>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</span>
								</a>
							</div>
							<div class="col-12 col-md-6">
								<a href="knowledge-study.php" class="article-banner x-half txt-right">
									<span class="box-img" style="background-image:url('assets/img/home-knowledge-thumb-01.jpg');"></span>
									<span class="box-txt">
										<span class="inner">
											<h3 class="headline txt-head">ถอดบทเรียน<br>จากโครงการ</h3>
											<span class="divider-h"></span>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</span>
								</a>
								<a href="knowledge-local.php" class="article-banner x-half txt-left">
									<span class="box-img" style="background-image:url('assets/img/home-knowledge-thumb-02.jpg');"></span>
									<span class="box-txt">
										<span class="inner">
											<h3 class="headline txt-head">ภูมิปัญญา<br>ท้องถิ่น</h3>
											<span class="divider-h"></span>
											<p class="txt-readmore">
												<strong class="txt">ดูรายละเอียด</strong>
												<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
											</p>
										</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- / KNOWLEDGE -->
		<!-- PR NEWS -->
		<section class="section section-prnews">
			<h2 class="head-section">สื่อประชาสัมพันธ์</h2>
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="article-container">
							<div class="row">
								<?php for ($i=0; $i < 3 ; $i++) { ?>
								<div class="col-12 col-md-4">
									<article class="article-item nowrap">
										<a href="news-pr-detail.php">
											<span class="date"><strong>16</strong>เม.ย. 2561</span>
											<span class="box-img">
												<img src="assets/img/_demo/article-thumb-0<?php echo $i+1; ?>.jpg" alt="โครงการ “พลังคนสร้างสรรค์โลก รวม
												พลังตามรอยพ่อของแผ่นดิน”">
											</span>
											<span class="box-txt">
												<h3 class="headline">โครงการ “พลังคนสร้างสรรค์โลก รวม
												พลังตามรอยพ่อของแผ่นดิน”</h3>
												<p class="summary">
													จิตพิสัยคันยิกรรมาชนวาไรตี้อพาร์ทเมนท์บลูเบอร์รีโครนาเซลส์อยุติธรรมฮิต ซิ่งซีดานซาร์ปอดแหก
													โปรเจกต์
												</p>
												<p class="txt-readmore">
													<strong class="txt">ดูรายละเอียด</strong>
													<i class="fa fa-angle-double-right right" aria-hidden="true"></i>
												</p>
											</span>
										</a>
									</article>
								</div>
								<?php } ?>
							</div>
							<p class="text-center">
								<a href="news-pr.php" class="btn-red"><strong class="txt">ดูทั้งหมด</strong> <i class="fa fa-angle-double-right right" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- PR NEWS -->
		<!-- CONTACT -->
		<section class="section">
			<div class="row no-gutters">
				<div class="col-12">
					<h2 class="head-section">ติดต่อเรา</h2>
					<div class="content">
						<div class="box-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d248057.18773201044!2d100.4930265717549!3d13.724893620202218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d6032280d61f3%3A0x10100b25de24820!2sBangkok!5e0!3m2!1sen!2sth!4v1530246906106" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
			<form action="" class="frm-contact">
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-12 col-lg-8">
							<div class="row">
								<div class="col-12"><h3 class="head">ติดต่อเรา</h3></div>
								<div class="col-12 col-md-6">
									<input type="text" class="input-txt" name="firstname" required placeholder="ชื่อ" />
								</div>
								<div class="col-12 col-md-6">
									<input type="text" class="input-txt" name="lastname" required placeholder="นามสกุล" />
								</div>
								<div class="col-12 col-md-6">
									<input type="email" class="input-txt" name="email" required placeholder="อีเมล" />
								</div>
								<div class="col-12 col-md-6">
									<input type="telephone" class="input-txt" name="telephone" required placeholder="หมายเลขโทรศัพท์" />
								</div>
								<div class="col-12">
									<textarea class="input-txt" name="message" rows="7" required placeholder="ข้อความ" rows="5"></textarea>
								</div>
								<div class="col-12">
									<p class="text-center">
										<button type="submit" class="btn-red">
											<span class="txt">ส่งข้อความ <i class="fa fa-angle-double-right right" aria-hidden="true"></i></span>
										</button>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</section>
		<!-- / CONTACT -->
	</main>
<?php include('inc/javascript.php'); ?>
<script>
	$(document).ready(function(){
		$('.herobanner .img-slider').slick({
			arrows:false,
			dots:true
		});
		$('.herobanner .event-slider').slick({
		  infinite:false,
		  arrows:false,
		  dots: true,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 576,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		  ],
		});
	});
</script>
<?php include('inc/footer.php'); ?>