<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<nav class="breadcrumb">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-lg-8">
						<a class="item" href="index.php">หน้าหลัก</a>
						<a class="item" href="event.php">ข่าวสารและกิจกรรม</a>
						<span class="item">ปลูกป่าทดแทนจังหวัดน่าน ครั้งที่ 3</span>
					</div>
				</div>
			</div>
		</nav>
		<article class="main-article">
			<section class="section">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-8">
							<figure class="main-img">
								<img src="assets/img/_demo/event-detail.jpg" alt="">
							</figure>
							<header class="header">
								<div class="row">
									<div class="col-12 col-md-auto">
										<div class="box-date-blue">30 เม.ย. 2561</div>
									</div>
									<div class="col-12 col-md-auto">
										<div class="hgroup">
											<h1 class="headline">ปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3</h1>
											<p class="txt">Today 6 PM · Yim Yam Hostel & Garden · Bangkok, Thailand 10220</p>
										</div>
										<p class="text-center text-md-left">
											<a href="#popup-info" class="btn-red fancybox-inline mr-md-3 mb-3"><strong class="txt">ข้อมูลสถานที่ </strong> <i class="fa fa-angle-double-right right" aria-hidden="true"></i></a>
											<a href="event-register.php" class="btn-red"><strong class="txt">สมัครกิจกรรม</strong> <i class="fa fa-angle-double-right right" aria-hidden="true"></i></a>
										</p>
									</div>
								</div>
							</header>
						</div>
					</div>
				</div>
				<div class="intro">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-12 col-lg-8">
								<h2>หมู่บ้านสุขสมบูรณ์ ต.หนองหัวโพ อ.หนองแซง จ.สระบุรี</h2>
								<p>
									เตรียมพบกับน้องๆ วง <strong>BNK48</strong> ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-8">
							<div class="body">
								<!-- text editor here -->
								<h2>ปลูกป่าทดแทน จังหวัดน่านครั้งที่  3</h2>
								<h3>ปลูกป่าทดแทน จังหวัดน่านครั้งที่  4</h3>
								<p>
									เตรียมพบกับน้องๆ วง <strong>BNK48</strong> ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน
								</p>
								<ul>
									<li>สยามพารากอน</li>
									<li>สยามพารากอน</li>
									<li>สยามพารากอน</li>
									<li>สยามพารากอน</li>
								</ul>
								<!-- / text editor here -->
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="section section-map mb-0">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-8">
							<h2 class="head txtC">แผนที่</h2>
							<div class="box-map">
								<img class="w-100" src="assets/img/_demo/map-02.jpg" alt="">
							</div>
							<p class="txtC">
								<a href="#" class="btn-red"><strong class="txt">นำทางใน Google</strong> <i class="fa fa-angle-double-right right" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</section>
		</article>
		<div style="display:none;">
			<div class="popup" id="popup-info">
				<div class="head-section dark txtL">วิธีการเดินทาง</div>
				<p>
					<strong>ขับรถไปเอง</strong><br>
					จากกรุงเทพฯ ไปสวนผึ้งได้ 2 เส้นทาง
					• เส้นทางที่ 1 ไปทางถ.ปิ่นเกล้า-นครชัยศรี สุดทางขึ้นสะพานต่างระดับเข้าสู่ ถ.เพชรเกษม ผ่าน อ.นครชัยศรี อ.เมือง จ.นครปฐม เข้าสู่ จ.ราชบุรี ผ่าน อ.เมือง อ.โพธาราม เลี้ยวขวาแถวเขางู เข้าสู่ทางหลวง 3087 ผ่าน อ.จอมบึง ตรงไปสู่ อ.สวนผึ้ง
					• ส่วนเส้นที่ 2 ใช้เส้นทางธนบุรี-ปากท่อ (ถ.พระราม 2) ผ่าน จ.สมุทรสาคร จ.สมุทรสงคราม ถึงสามแยกวังมะนาว เลี้ยวขวาไปทาง จ.ราชบุรี ผ่าน อ.ปากท่อ อ.เมือง
					ผ่านเขาแก่นจันทร์ แล้วเลี้ยวซ้าย เข้าทางหลวง 3208 ไปสู่ อ.สวนผึ้ง ใช้เวลาประมาณ 2 ชั่วโมงกว่าๆ
				</p>
				<p>
					<strong>รถไฟ</strong><br>
					รถไฟออกจากสถานีกรุงเทพฯ (หัวลำโพง) และสถานีรถไฟธนบุรี (บางกอกน้อย) ทุกวัน ซึ่งมีทั้งรถ
					ด่วน รถเร็ว รถธรรมดา ใช้เวลาเดินทางประมาณ 2 ชั่วโมง รายละเอียดและกำหนดเวลาตลอดจนค่า
					โดยสาร สามารถตรวจสอบเวลาเดินรถไฟได้ที่ <a href="http://www.railway.co.th">www.railway.co.th</a>
				</p>
				<p>
					<strong>รถประจำทาง</strong><br>
					สามารถขึ้นรถได้ที่สถานีขนส่งสายใต้ใหม่ มาลงที่ อ.จอมบึง จ.ราชบุรี (สามารถเช็คเที่ยวรถได้ที่ <a href="http://www.transport.co.th">www.transport.co.th</a>) หลังจากที่มาถึงที่ อ.จอมบึงแล้วนั้น คุณสามารถนั่งรถโดยสารต่อมายัง อ.สวนผึ้ง โดยรถประจำทางสายจอมบึง - สวนผึ้ง (คันสีนํ้าเงิน) หรือนั่งรถมอเตอร์ไซค์รับจ้าง จาก
					ตลาด อ.สวนผึ้ง
				</p>
				<p>
					<strong>รถตู้</strong><br>
					สำหรับใครที่ไม่มีรถส่วนตัวหรืออยากแบ็คแพ็คเกอร์ไปเอง สามารถไปขึ้นรถได้ที่ห้างเซ็นจูรี่ อนุสาวรีย์
					ชัยสมรภูมิ ปลายทางชัฏป่าหวาย อ.สวนผึ้ง ค่าโดยสาร 140 บาท / คน (รถเหมาไปต่อสวนผึ้งคิด 200 บาท) จากนั้นสามารถต่อรถโดยสารประจำทาง, รถมอเตอร์ไซด์ไปลงสวนผึ้งอีกที รถเริ่มตั้งแต่เวลา 05.00 น. ไปจนถึง 18.30 น. รถออกทุกชั่วโมงครึ่ง เวลาเดินรถ เวลาเดียวกันทั้งจากกรุงเทพฯ และชัฏป่าหวาย สอบถามรายละเอียดเพิ่มเติมได้ที่ 08-0613-3848 หรือ 08-2249-4010
				</p>
				<p>
					<strong>ที่พักสำหรับผู้เข้าร่วมกิจกรรม</strong><br>
					ผู้เข้าร่วมกิจกรรมสามารถพักค้างคืนได้ที่วัดหนองขาม ต.ป่าหวาย อ.สวนผึ้ง จ.ราชบุรี ซึ่งอยู่ห่างจาก
					พื้นที่ทำกิจกรรม ณ ไร่สุขกลางใจ 1 กิโลเมตร (ขอให้นำเต็นท์นอนมาเอง)
				</p>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>