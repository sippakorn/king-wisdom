<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<div class="media-theater">
			<div class="media-body">
				<div class="container">
					<div class="row justify-content-center">
							<div class="col-12 col-lg-10 col-xl-8">
								<a href="" class="btn-back">ย้อนกลับ</a>
								<div class="media-view-container">
									<div class="media-view">
										<img src="assets/img/_demo/media-detail.jpg" alt="">
									</div>
									<a href="#" class="btn-nav-prev"></a>
									<a href="#" class="btn-nav-next"></a>
								</div>
								<div class="media-list-container">
									<div class="media-list-slider">
										<div class="item"><a href=""><img src="assets/img/_demo/media-detail-thumb.jpg" alt=""></a></div>
										<div class="item"><a href=""><img src="assets/img/_demo/media-detail-thumb.jpg" alt=""></a></div>
										<div class="item"><a href=""><img src="assets/img/_demo/media-detail-thumb.jpg" alt=""></a></div>
										<div class="item"><a href=""><img src="assets/img/_demo/media-detail-thumb.jpg" alt=""></a></div>
										<div class="item"><a href=""><img src="assets/img/_demo/media-detail-thumb.jpg" alt=""></a></div>
										<div class="item"><a href=""><img src="assets/img/_demo/media-detail-thumb.jpg" alt=""></a></div>
										<div class="item"><a href=""><img src="assets/img/_demo/media-detail-thumb.jpg" alt=""></a></div>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
			<div class="media-detail">
				<div class="container">
					<div class="row">
						<div class="col-12 col-md-6 col-lg-5 offset-lg-1 col-xl-4 offset-xl-2">
							<div class="box-detail">
								<p>
									เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่ง
									มอบความสุขให้กับทุกๆ ท่าน ในงาน“แบง
									ค็อก คอมิค คอน และ ไทยแลนด
								</p>
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-4 offset-lg-1 col-xl-3 offset-xl-1">
							<div class="box-tag">
								<p><strong>Tag</strong> <i class="fa fa-question-circle-o" aria-hidden="true"></i></p>
								<ul class="tag-list">
									<li><a href="#">ศาสตร์พระราชา</a></li>
									<li><a href="#">ดร.เกริก</a></li>
									<li><a href="#">คำสอน</a></li>
									<li><a href="#">พอเพียง</a></li>
									<li><a href="#">ศาสตร์พระราชา</a></li>
									<li><a href="#">คำสอน</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<script>
	$(document).ready(function(){
		$('.media-list-slider').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		  ],
		});
	});
</script>
<?php include('inc/footer.php'); ?>