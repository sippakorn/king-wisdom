<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<div class="header-page-banner">
			<div class="inner">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-xl-10">
							<h1 class="head-page txtC">รู้จักโครงการ</h1>
							<ul class="page-tab-selection">
								<li class="active"><a href="about-pakee.php">ภาคีเครือข่าย</a></li>
								<li><a href="about-konmeejai.php">คนมีใจ</a></li>
							</ul>
							<div class="page-intro">
								<div class="box-img">
									<div class="img-item">
											<img class="img" src="assets/img/about-pakee-thumb.jpg" alt="">
											<img class="txt" src="assets/img/txt-arrow_pakee.png" alt="ภาคีเครือข่าย">
									</div>
									<img class="aw" src="assets/img/aw_intro-about-pakee.png"/>
								</div>
								<div class="box-txt">
									<h2 class="head">ภาคีเครือข่าย</h2>
									<p class="summary">
										<strong>โครงการ “พลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของ
										แผ่นดิน”</strong> ก่อตั้งขึ้นในปี พ.ศ. 2556  โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน พระบาทสมเด็จพระปรมินทรมหาภูมิพล
										อดุลยเดช ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ
										ป่าสัก ก่อตั้งขึ้นในปี พ.ศ. 2556 <br><br>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-quote">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-xl-9 txtC">
							<blockquote>
								โครงการ “พลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของแผ่นดิน” ก่อตั้งขึ้นในปี พ.ศ. 2556 โดยได้รับแรงบันดาลใจจากกระแสพระราชดำรัสใน พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช
							</blockquote>
							<p>
								โครงการ “พลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของแผ่นดิน” ก่อตั้งขึ้นในปี พ.ศ. 2556 โดยได้รับแรง
								บันดาลใจจากกระแสพระราชดำรัสในพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดชที่ทรงห่วงใยต่อปัญหา
								น้ำท่วมและภัยแล้งบริเวณลุ่มน้ำป่าสัก ก่อตั้งขึ้นในปี พ.ศ. 2556  
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-xl-9">
					<ul class="about-list-container">
						<li class="item">
							<div class="box-img">
								<img src="assets/img/_demo/about-list-thumb-01.jpg" alt="">
							</div>
							<div class="box-txt">
								<p>
									<strong>รายการเจาะใจ</strong><br>
									เตรียมพบกับน้องๆ วง BNK48ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน
								</p>
							</div>
						<li class="item">
							<div class="box-img">
								<img src="assets/img/_demo/about-list-thumb-02.jpg" alt="">
							</div>
							<div class="box-txt">
								<p>
									<strong>คืนชีวิตให้แผ่นดิน</strong><br>
									เตรียมพบกับน้องๆ วง BNK48ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน
								</p>
							</div>
						<li class="item">
							<div class="box-img">
								<img src="assets/img/_demo/about-list-thumb-03.jpg" alt="">
							</div>
							<div class="box-txt">
								<p>
									<strong>คืนชีวิตให้แผ่นดิน</strong><br>
									เตรียมพบกับน้องๆ วง BNK48ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน
								</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>