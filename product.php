<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<div class="header-page-banner">
			<div class="inner">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-xl-10">
							<h1 class="head-page txtC">สนใจสั่งซื้อ</h1>
							<div class="page-intro">
								<div class="box-img">
									<div class="img-item">
											<img class="img" src="assets/img/product-thumb.jpg" alt="">
											<img class="txt" src="assets/img/txt-arrow_product.png" alt="ธรรมธุรกิจ">
									</div>
									<img class="aw" src="assets/img/aw_intro-product.png"/>
								</div>
								<div class="box-txt">
									<h2 class="head">สนใจสั่งซื้อ</h2>
									<p class="summary">
										<strong>โครงการ “พลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของ
										แผ่นดิน”</strong> ก่อตั้งขึ้นในปี พ.ศ. 2556  โดยได้รับแรงบันดาลใจจาก
										กระแสพระราชดำรัสใน พระบาทสมเด็จพระปรมินทรมหาภูมิพล
										อดุลยเดช ที่ทรงห่วงใยต่อปัญหาน้ำท่วมและภัยแล้งบริเวณลุ่มน้ำ
										ป่าสัก ก่อตั้งขึ้นในปี พ.ศ. 2556 <br><br>
									</p>
									<!-- <p class="text-center text-lg-left">
										<a href="#" class="btn-red"><strong>สนใจสั่งซื้อสินค้าจากเครือข่าย</strong> <i class="fa fa-angle-double-right right" aria-hidden="true"></i></a>
									</p>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br class="d-none d-sm-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="product-container">
						<div class="row">
							<?php $product = array('บริษัท เชฟรอนประเทศไทยสำรวจและผลิต จำกัด','สถาบันเศรษฐกิจพอเพียง','มูลนิธิกสิกรรมธรรมชาติ','สถาบันเทคโนโลยีพระจอมเกล้า<br>เจ้าคุณทหารลาดกระบัง','รายการเจาะใจ'); ?>
							<?php foreach ($product as $k => $v) { ?>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="product-item">
									<p class="img"><img src="assets/img/product/<?php echo $k+1; ?>.svg" alt="<?php echo $v; ?>"></p>
									<p class="txt"><?php echo $v; ?></p>
									<p class="box-btn">
										<a href="#" class="btn-red">สนใจสั่งซื้อสินค้าจากเครือข่าย</a>
									</p>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>