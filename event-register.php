<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container event-register" role="main">
		<nav class="breadcrumb">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-lg-8">
						<a class="item" href="index.php">หน้าหลัก</a>
						<a class="item" href="event.php">ข่าวสารและกิจกรรม</a>
						<a class="item" href="#">ปลูกป่าทดแทนจังหวัดน่าน ครั้งที่ 3</a>
						<span class="item">สมัครกิจกรรม</span>
					</div>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 offset-lg-2">
					<header class="header">
						<h1 class="head-section txt-head">สมัครกิจกรรม</h1>
						<p><strong>ปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3</strong><br>โครงการพลังคนสร้างสรรค์โลก รวมพลังตามรอยพ่อของแผ่นดิน ปี 5</p>
					</header>
					<form action="" class="form" id="form-register" novalidate>
						<div class="row">
							<div class="col-12 col-lg-6">
								<input type="text" class="input-txt" name="firstname" placeholder="ชื่อ*" />
							</div>
							<div class="col-12 col-lg-6">
								<input type="text" class="input-txt" name="lastname" placeholder="นามสกุล*" />
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-lg-6">
								<input type="text" class="input-txt" name="nickname" placeholder="ชื่อเล่น" />
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<textarea name="address" rows="7" class="input-txt" placeholder="ที่อยู่"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-lg-6">
								<select name="province" id="" class="input-txt">
									<option value="0">เลือกจังหวัด</option>
									<option value="1">กรุงเทพมหานคร</option>
								</select>
							</div>
							<div class="col-12 col-lg-6">
								<input type="number" class="input-txt" name="age" placeholder="อายุ*" />
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-lg-6">
								<input type="telephone" class="input-txt" name="mobile" placeholder="เบอร์โทรศัพท์มือถือ*" />
							</div>
							<div class="col-12 col-lg-6">
								<input type="telephone" class="input-txt" name="telephone" placeholder="เบอร์โทรศัพท์บ้านหรือที่ทำงาน" />
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-lg-6">
								<input type="email" class="input-txt" name="email" placeholder="อีเมล*" />
							</div>
							<div class="col-12 col-lg-6">
								<input type="text" class="input-txt" name="lineID" placeholder="Line ID" />
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>มาจากหน่วยงานใด<span class="txt-red">*</span></p>
								<p>
									<label class="label-item" for="radio1-1"><input type="radio" name="radio1" id="radio1-1"/><span class="txt">บุคคลทั่วไป</span></label>
									<label class="label-item" for="radio1-2"><input type="radio" name="radio1" id="radio1-2"/><span class="txt">สถาบันเศรษฐกิจพอเพียง</span></label>
									<label class="label-item" for="radio1-3"><input type="radio" name="radio1" id="radio1-3"/><span class="txt">เครือข่ายกสิกรรมธรรมชาติ</span></label><br>
									<label class="label-item" for="radio1-4"><input type="radio" name="radio1" id="radio1-4"/><span class="txt">อื่นๆ</span></label>
									<span class="box-input short"><input type="text" class="input-txt" /></span>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>ท่านต้องการค้างคืนในวันใด<span class="txt-red">*</span> <small><i>(สำหรับท่านที่ต้องการค้างคืนในพื้นที่ ที่ทางโครงการฯ เตรียมไว้ให้ขอให้นำเต็นท์นอนมาเอง)</i></small></p>
								<div class="row">
									<div class="col-12 col-lg-6">
										<label class="label-item" for="radio2-1"><input type="radio" name="radio2" id="radio2-1"/><span class="txt">ไม่ค้างคืนในพื้นที่ที่โครงการฯ เตรียมไว้ให้</span></label>
									</div>
									<div class="col-12 col-lg-6">
										<label class="label-item" for="radio2-2"><input type="radio" name="radio2" id="radio2-2"/><span class="txt">ค้างคืนวันศุกร์ที่ 18 ส.ค. / วันเสาร์ที่ 19 ส.ค. 2561</span></label>
									</div>
									<div class="col-12 col-lg-6">
										<label class="label-item" for="radio2-3"><input type="radio" name="radio2" id="radio2-3"/><span class="txt">ค้างคืนวันศุกร์ที่ 18 ส.ค. 2561</span></label>
									</div>
									<div class="col-12 col-lg-6">
										<label class="label-item" for="radio2-4"><input type="radio" name="radio2" id="radio2-4"/><span class="txt">ค้างคืนเสาร์ที่ 19 ส.ค. 2561</span></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>ประเภทอาหารที่แพ้ หรือ ยาที่แพ้ หรือโรคประจำตัว<span class="txt-red">*</span> <small><i>(ถ้ามีโปรดระบุในช่องอื่นๆ)</i></small></p>
								<p>
									<label class="label-item" for="radio3-1"><input type="radio" name="radio3" id="radio3-1"/><span class="txt">ไม่มี</span></label><br>
									<label class="label-item" for="radio3-2"><input type="radio" name="radio3" id="radio3-2"/><span class="txt">อื่นๆ</span></label>
									<span class="box-input short"><input type="text" class="input-txt" /></span>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>ท่านต้องการขับรถยนต์ส่วนตัวไปเองหรือไม่<span class="txt-red">*</span></p>
								<p>
									<label class="label-item" for="radio4-1"><input type="radio" name="radio4" id="radio4-1"/><span class="txt">ขับรถยนต์ไปเอง</span></label>
									<label class="label-item" for="radio4-2"><input type="radio" name="radio4" id="radio4-2"/><span class="txt">ไม่ขับรถยนต์ไป</span></label>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>example checkbox<span class="txt-red">*</span></p>
								<p>
									<label class="label-item" for="checkbox-1"><input type="checkbox" name="checkbox" id="checkbox-1" value="checkbox-1"/><span class="txt">checkbox 1</span></label>
									<label class="label-item" for="checkbox-2"><input type="checkbox" name="checkbox" id="checkbox-2" value="checkbox-2"/><span class="txt">checkbox 2</span></label>
									<label class="label-item" for="checkbox-3"><input type="checkbox" name="checkbox" id="checkbox-3" value="checkbox-3"/><span class="txt">checkbox 3</span></label>
									<label class="label-item" for="checkbox-4"><input type="checkbox" name="checkbox" id="checkbox-4" value="checkbox-4"/><span class="txt">checkbox 4</span></label>
								</p>
							</div>
						</div>
						<p class="txtC">
							<button type="submit" class="btn-red">สมัครกิจกรรม <i class="fa fa-angle-double-right right" aria-hidden="true"></i></button>
						</p>
						<br>
					</form>
				</div>
			</div>
		</div>
		<div class="contact-bar bg-white">
			<br>
			<p class="col txtC">
				สอบถามรายละเอียดเพิ่มเติมโทร : 092-768-2885 หรือสอบถามได้ที่ <span class="box-social dark"><a href="#" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
			</p>
			<br>
		</div>
	</main>
	<div style="display:none;">
		<div class="popup popup-success" id="popup-success">
			<h1 class="head-section dark txtC">สมัครกิจกรรมเรียบร้อย</h1>
			<p class="txtC">
				คุณได้สมัครกิจกรรม <strong>“โครงการปลูกป่าทดแทน จังหวัดน่านครั้งที่ 3”</strong> โครงการพลังคนสร้างสรรค์
				โลก รวมพลังตามรอยพ่อของแผ่นดิน ปี 5 โปรดตรวจสอบข้อมูลรายละเอียดของโครงการ ได้ที่อีเมล
				ของคุณ ทางโครงการได้ส่งข้อมูลให้ทางอีเมล
			</p>
			<p class="txtC">
				<img class="mx-auto" src="assets/img/logo.png" alt="" width="220" />
			</p>
		</div>
	</div>
<?php include('inc/javascript.php'); ?>
<script>
	$(document).ready(function(){
		$('#form-register').submit(function(e){
			var checkRegister = fn_checkRegister();
			if(checkRegister){
				popupSuccess();
			}
			e.preventDefault();
		});
	});
	var form = '#form-register'
	var emailCheck = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var phoneCheck = /^-{0,1}\d*\.{0,1}\d+$/;
	function fn_checkInput($elm){
		$(form).find('.error').removeClass('error');
		$elm.addClass('error').focus();
	}
	function fn_checkRegister(){
		if($(form+' [name="firstname"]').val() == 0){
			fn_checkInput($(form+' [name="firstname"]'));
			alert('กรุณากรอกชื่อ');
			return false;
		} else if($(form+' [name="lastname"]').val() == 0){
			fn_checkInput($(form+' [name="lastname"]'));
			alert('กรุณากรอกนามสกุล');
			return false;
		} else if($(form+' [name="age"]').val() == ""){
			fn_checkInput($(form+' [name="age"]'));
			alert('กรุณากรอกอายุ');
			return false;
		} else if($(form+' [name="mobile"]').val() == 0){
			fn_checkInput($(form+' [name="mobile"]'));
			alert('กรุณาใส่เบอร์โทรศัพท์');
			return false;
		} else if(!(phoneCheck.test($(form+' [name="mobile"]').val()))){
			fn_checkInput($(form+' [name="mobile"]'));
			alert('กรุณาใส่เบอร์โทรศัพท์ด้วยตัวเลขเท่านั้น');
			return false;
		} else if ($(form+' [name="mobile"]').val().toString().length < '9') {
			fn_checkInput($(form+' [name="mobile"]'));
			alert('กรุณาใส่เบอร์โทรศัพท์ให้ครบถ้วน');
			return false;
		} else if($(form+' [name="email"]').val() == 0){
			fn_checkInput($(form+' [name="email"]'));
			alert('กรุณากรอกอีเมล์');
			return false;
		} else if(!(emailCheck.test($(form+' [name="email"]').val()))){
			fn_checkInput($(form+' [name="email"]'));
			alert('กรุณากรอกอีเมล์ให้ถูกต้อง');
			return false;
		} else if(!$(form+' [name="radio1"]').is(':checked')){
			fn_checkInput($(form+' [name="radio1"]'));
			alert('กรุณาเลือกข้อมูลให้ครบถ้วน');
			return false;
		}  else if(!$(form+' [name="radio2"]').is(':checked')){
			fn_checkInput($(form+' [name="radio2"]'));
			alert('กรุณาเลือกข้อมูลให้ครบถ้วน');
			return false;
		}  else if(!$(form+' [name="radio3"]').is(':checked')){
			fn_checkInput($(form+' [name="radio3"]'));
			alert('กรุณาเลือกข้อมูลให้ครบถ้วน');
			return กรุณาเลือกข้อมูลให้ครบถ้วนผ;
		}  else if(!$(form+' [name="radio4"]').is(':checked')){
			fn_checkInput($(form+' [name="radio4"]'));
			alert('กรุณาเลือกข้อมูลให้ครบถ้วน');
			return false;
		} else {
			$(form+' .error').removeClass('error');
			return true;
		}
	}
	function popupSuccess(){
		$.fancybox({
			href:'#popup-success',
		});
	}
</script>
<?php include('inc/footer.php'); ?>