<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<nav class="breadcrumb">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-lg-8">
						<a class="item" href="index.php">หน้าหลัก</a>
						<a class="item" href="#">รู้จักโครงการ</a>
						<a class="item" href="about-konmeejai.php">คนมีใจ</a>
						<span class="item">ดร. เกริก มีมุ่งกิจ</span>
					</div>
				</div>
			</div>
		</nav>
		<article class="main-article">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-lg-8">
						<figure class="main-img">
							<img class="w-100" src="assets/img/_demo/about-detail.jpg" alt="">
						</figure>
						<header class="header">
							<h1 class="headline">ดร. เกริก มีมุ่งกิจ</h1>
							<h2 class="subhead">ผู้ก่อตั้งวนเกษตรเขาฉกรรจ์</h2>
						</header>
						<div class="body">
							<!-- text editor here -->
							<p>
								เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน
							</p>
							<p style="float:left;">
								<img src="assets/img/_demo/about-detail-02.jpg" alt="">
							</p>
							<p>
								เตรียมพบกับน้องๆ วง BNK48ที่จะมาส่งมอบความ
								สุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยาม
								พารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมา
								ส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ ร อยัล พารากอน ฮอลล์ สยามพารากอนเตรียมพบกับน้องๆ วงBNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน“แบง
								ค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน
							</p>
							<p>
								เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน เตรียมพบกับน้องๆ วง BNK48 ที่จะมาส่งมอบความสุขให้กับทุกๆ ท่าน ในงาน “แบงค็อก คอมิค คอน และ ไทยแลนด์ คอมิค คอน 2018 (Bangkok Comic Con & Thailand Comic Con 2018)” วันที่ 27-29 เมษายน นี้ ที่่ รอยัล พารากอน ฮอลล์ สยามพารากอน
							</p>
							<!-- / text editor here -->
						</div>
						<div class="footer">
							<p class="box-share txtR">
								<strong>SHARE : </strong>
								<a href="#" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="#" class="item"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								<a href="#" class="item"><img src="assets/img/icon_line.png" alt="Line"></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<section class="section-map txtC">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-8">
							<h2 class="head">แผนที่</h2>
							<div class="box-map">
								<img class="w-100" src="assets/img/_demo/map-02.jpg" alt="">
							</div>
							<p>
								<a href="#" class="btn-red"><strong class="txt">นำทางใน Google</strong> <i class="fa fa-angle-double-right right" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</section>
		</article>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>