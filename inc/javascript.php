	<footer class="main-footer">
		<nav class="footer-nav">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<ul class="menu">
							<li class="group">
								<div class="menu-item">
									<a href="index.php" class="head">หน้าแรก</a>
								</div>
								<div class="menu-item">
									<a href="#" class="head">รู้จักโครงการ</a>
									<ul class="submenu">
										<li><a href="about-pakee.php">ภาคีเครือข่าย</a></li>
										<li><a href="about-konmeejai.php">คนมีใจ</a></li>
									</ul>
								</div>
							</li>
							<li class="group">
								<div class="menu-item">
									<a href="#" class="head">ข่าวสารและกิจกรรม</a>
									<ul class="submenu">
										<li><a href="event.php">กิจกรรมที่จะเกิดขึ้น</a></li>
										<li><a href="event-done.php">กิจกรรมที่ผ่านมา</a></li>
									</ul>
								</div>
							</li>
							<li class="group">
								<div class="menu-item">
									<a href="#" class="head">คลังความรู้</a>
									<ul class="submenu">
										<li><a href="knowledge-king.php">ศาสตร์พระราชา</a></li>
										<li><a href="knowledge-study.php">ถอดบทเรียนจากโครงการ</a></li>
										<li><a href="knowledge-local.php">ภูมิปัญญาท้องถิ่น</a></li>
									</ul>
								</div>
							</li>
							<li class="group">
								<div class="menu-item">
									<a href="#" class="head">สื่อประชาสัมพันธ์</a>
									<ul class="submenu">
										<li><a href="news-pr.php">Press Release</a></li>
										<li><a href="news-vdo.php">VDO Content</a></li>
									</ul>
								</div>
							</li>
							<li class="group sublast">
								<div class="menu-item">
									<a href="contact-us.php" class="head">ติดต่อเรา</a>
								</div>
								<div class="menu-item">
									<a href="product.php" class="head">สนใจสั่งซื้อ</a>
								</div>
							</li>
							<li class="group last">
								<div class="box-logo">
									<span class="item"><img src="assets/img/logo_Shevron-01.png" alt=""></span>
									<span class="item"><img src="assets/img/logo_IOSE-01.png" alt=""></span>
									<span class="item"><img src="assets/img/logo_มูลนิธิโลโก้-w-01.png" alt=""></span>
									<span class="item"><img src="assets/img/logo_ลาดกระบังโลโก้-w-01.png" alt=""></span>
								</div>
								<div class="box-social">
									<div class="head">ติดตามเรา</div>
									<a href="#" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
									<a href="#" class="item"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
									<a href="#" class="item"><img src="assets/img/icon_line-gray.png" alt="Line"></a>
									<a href="#" class="item"><i class="fa fa-instagram" aria-hidden="true"></i></a>
									<a href="#" class="item"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="copyright-bar">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="copyright">
							<small>
								© <?php echo date('Y'); ?> COPYRIGHT <a href="#">AJOURNEYINSPIREDBYTHEKING</a> ALL RIGHT RESERVED.
							</small>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>
<script src="<?php echo $site['url']; ?>assets/js/core/jquery.easing.js"></script>
<script src="<?php echo $site['url']; ?>assets/js/core/jquery.mousewheel.js"></script>
<script src="<?php echo $site['url']; ?>assets/js/core/detectmobilebrowser.js"></script>
<script src="<?php echo $site['url']; ?>assets/js/vendor/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?php echo $site['url']; ?>assets/js/vendor/slick/slick.min.js"></script>
<script src="<?php echo $site['url']; ?>assets/js/main.min.js<?php echo $site['cache_version']; ?>"></script>