<header class="main-header">
	<div class="top-bar">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-auto">
					<a href="index.php" class="logo"><img src="assets/img/logo.png" alt=""></a>
				</div>
				<div class="col d-none d-lg-block">
					<div class="box-right">
						<form action="" class="form frm-search">
							<input type="text" class="input-txt" name="keyword" placeholder="ค้นหาข้อมูล" />
							<button type="submit" class="btn-search"></button>
						</form>
						<div class="lang-selection">
							<a href="#" class="active">TH</a>
							<span class="divider"></span>
							<a href="#">EN</a>
						</div>
					</div>
				</div>
				<div class="col d-block d-lg-none">
					<a href="javascript:void(0);" class="btn-menu"><span></span></a>
				</div>
			</div>
		</div>
	</div>
	<nav class="main-nav" role="navigation">
		<a href="javascript:void(0);" class="btn-menu-close" title="Close Menu"></a>
		<div class="inner">
			<div class="lang-selection d-block d-lg-none">
				<a href="#" class="active">TH</a>
				<span class="divider"></span>
				<a href="#">EN</a>
			</div>
			<div class="menu">
				<a href="about-pakee.php">รู้จักโครงการ</a>
				<a href="event.php">ข่าวสารและกิจกรรม</a>
				<a href="knowledge-king.php">คลังความรู้</a>
				<a href="news-pr.php">สื่อประชาสัมพันธ์</a>
				<a href="contact-us.php">ติดต่อเรา</a>
				<a href="product.php">สนใจสั่งซื้อ</a>
			</div>
			<div class="box-social d-block d-lg-none">
				<a href="#" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a href="#" class="item"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
				<a href="#" class="item"><img src="assets/img/icon_line-blue.png" alt="Line"></a>
				<a href="#" class="item"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="#" class="item"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			</div>
		</div>
	</nav>
</header>