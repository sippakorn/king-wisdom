<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container contact" role="main">
		<div class="container-full">
			<div class="row no-gutters">
				<div class="col-12 col-lg-6">
					<div class="box-map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d248057.18773201044!2d100.4930265717549!3d13.724893620202218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d6032280d61f3%3A0x10100b25de24820!2sBangkok!5e0!3m2!1sen!2sth!4v1530246906106" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<form action="" class="frm-contact">
						<div class="inner">
							<div class="row">
								<div class="col-12"><h1 class="head">ติดต่อเรา</h1></div>
								<div class="col-12 col-md-6">
									<input type="text" class="input-txt" name="firstname" required placeholder="ชื่อ" />
								</div>
								<div class="col-12 col-md-6">
									<input type="text" class="input-txt" name="lastname" required placeholder="นามสกุล" />
								</div>
								<div class="col-12 col-md-6">
									<input type="email" class="input-txt" name="email" required placeholder="อีเมล" />
								</div>
								<div class="col-12 col-md-6">
									<input type="text" class="input-txt" name="telephone" required placeholder="หมายเลขโทรศัพท์" />
								</div>
								<div class="col-12">
									<textarea class="input-txt" name="message" rows="7" required placeholder="ข้อความ" rows="5"></textarea>
								</div>
								<div class="col-12">
									<p class="text-center">
										<button type="submit" class="btn-red">
											<span class="txt">ส่งข้อความ <i class="fa fa-angle-double-right right" aria-hidden="true"></i></span>
										</button>
									</p>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>